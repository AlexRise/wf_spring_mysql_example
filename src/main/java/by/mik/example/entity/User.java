package by.mik.example.entity;

import javax.persistence.*;

/**
 * Created by sia on 07.07.2016.
 */
@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long _id;

    private String name;

//    private String email;
//
//    private String password;
//
//    private TypeUser typeUser;

    public User() {

    }

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return _id;
    }
}
