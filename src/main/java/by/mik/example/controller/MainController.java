package by.mik.example.controller;

//import by.mik.example.entity.User;
import by.mik.example.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;



@Controller
public class MainController {

    @PersistenceContext
    private EntityManager em;


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView main(@ModelAttribute("userJSP") User user) {
        user = em.find(User.class, 1l);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("userJSP", user);
        modelAndView.setViewName("index");
        return modelAndView;
    }
}
